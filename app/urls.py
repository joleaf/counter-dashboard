import os

from django.contrib.auth.decorators import permission_required
from django.urls import path

from app.views import not_implemented, IndexView, DashboardView, ApiDashboard, ApiCreateDashboard, ApiCounterGroup, \
    ApiCounter, ApiCounterTrigger, ApiEventsPage, ApiEventsYear, ApiCounterGroupEventsYear, CounterDashboardLoginView

if os.environ.get("REQUIRE_LOGIN", False):
    check_permission = permission_required('app.counter_dashboard.add_counter_dashboard', login_url='/login/')
else:
    check_permission = lambda x: x

urlpatterns = [
    # View for Creating a new Dashboard
    path('', check_permission(IndexView.as_view()), name="index"),
    #  View for Login
    path('login/', CounterDashboardLoginView.as_view(),name='login'),
    # View for viewing a dashboard
    path('dashboard/<uuid:uuid>/', DashboardView.as_view(), name="dashboard"),
    # API Create new dashboard
    path('api/dashboard/', check_permission(ApiCreateDashboard.as_view()), name='api_create_dashboard'),
    # API: Get,Update,Delete Dashboard
    path('api/dashboard/<uuid:uuid>/', ApiDashboard.as_view(), name='api_dashboard'),
    # API: Get,Post,Delete Admins,Worker,Viewer TODO: Later; for now, just use the default uuids
    path('api/dashboard/<uuid:uuid>/users/', not_implemented),
    # API: Get (list), Post (Create) a CounterGroup
    path('api/dashboard/<uuid:uuid>/countergroup/', ApiCounterGroup.as_view()),
    # API: Get,Post(Update),Delete CounterGroup
    path('api/dashboard/<uuid:uuid>/countergroup/<int:id>/', ApiCounterGroup.as_view()),
    # API: Post (Create) a Counter
    path('api/dashboard/<uuid:uuid>/counter/', ApiCounter.as_view()),
    # API: Get (a single) counter, Post (Update a counter, only Admin Fields, no event), Delete Counter
    path('api/dashboard/<uuid:uuid>/counter/<int:id>/', ApiCounter.as_view()),
    # API: Post (Update increment the counter)
    path('api/dashboard/<uuid:uuid>/counter/<int:id>/trigger/', ApiCounterTrigger.as_view()),
    # API: Get Events for specific counter with pagination
    path('api/dashboard/<uuid:uuid>/counter/<int:id>/events/page/<int:page>/', ApiEventsPage.as_view()),
    # API: Get Events for specific counter with year
    path('api/dashboard/<uuid:uuid>/counter/<int:id>/events/year/<int:year>/', ApiEventsYear.as_view()),
    # API: Get Events for specific countergroup with pagination
    path('api/dashboard/<uuid:uuid>/countergroup/<int:id>/events/page/<int:page>/', not_implemented),
    # API: Get Events for specific countergroup with year
    path('api/dashboard/<uuid:uuid>/countergroup/<int:id>/events/year/<int:year>/',
         ApiCounterGroupEventsYear.as_view()),
]
