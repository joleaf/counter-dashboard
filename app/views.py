import json

from django.contrib.auth.views import LoginView
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.http import Http404, HttpResponse, JsonResponse, HttpResponseForbidden, HttpRequest
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from app.forms import CreateDashboardForm
from app.models import CounterDashboard, CounterUser, CounterGroup, Counter, Event


def not_implemented(request):
    raise Http404


class IndexView(View):
    def get(self, request: HttpRequest):
        form = CreateDashboardForm()
        context = {'form': form}
        return render(request, 'app/index.html', context)

    def post(self, request: HttpRequest):
        form = CreateDashboardForm(request.POST)
        if not form.is_valid():
            context = {'form': form}
            return render(request, 'app/index.html', context)
        # Create a new dashboard
        new_counter_dashboard = CounterDashboard(name=form.data.get("name"))
        new_counter_dashboard.save()
        # Get Admin uuid
        counter_admin = new_counter_dashboard.counteradmin_set.first()
        return redirect('dashboard', uuid=counter_admin.uuid)


class CounterDashboardLoginView(LoginView):
    redirect_authenticated_user = True
    template_name = 'app/login.html'



class DashboardView(View):
    def get(self, request: HttpRequest, uuid: str):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return redirect('index')
        context = {
            'counter_dashboard': user.counter_dashboard,
            'user': user
        }
        return render(request, 'app/dashboard.html', context)


@method_decorator(csrf_exempt, name='dispatch')
class ApiCreateDashboard(View):
    def post(self, request: HttpRequest):
        request_data = json.loads(request.body)
        name = "New Dashboard"
        if request_data.get('name', None):
            name = request_data.get('name')
        counter_dashboard = CounterDashboard(name=name)
        counter_dashboard.save()
        return JsonResponse(counter_dashboard.as_dict(add_user_infos=True))


@method_decorator(csrf_exempt, name='dispatch')
class ApiDashboard(View):
    def get(self, request: HttpRequest, uuid: str):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        return JsonResponse(user.counter_dashboard.as_dict(add_user_infos=user.is_counter_admin()))

    def post(self, request: HttpRequest, uuid: str):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_admin():
            return HttpResponseForbidden()
        counter_dashboard = user.counter_dashboard
        request_data = json.loads(request.body)
        if request_data.get('name', None):
            counter_dashboard.name = request_data.get('name')
        counter_dashboard.save()
        return JsonResponse(counter_dashboard.as_dict(add_user_infos=user.is_counter_admin()))

    def delete(self, request: HttpRequest, uuid: str):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_admin():
            return HttpResponseForbidden()
        user.counter_dashboard.delete()
        return HttpResponse(status=204)


@method_decorator(csrf_exempt, name='dispatch')
class ApiCounterGroup(View):
    def get(self, request: HttpRequest, uuid: str, id: int = None):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if id is None:
            return JsonResponse(
                [counter_group.as_dict() for counter_group in user.counter_dashboard.countergroup_set.all()],
                safe=False)
        try:
            return JsonResponse(user.counter_dashboard.countergroup_set.get(id=id).as_dict())
        except ObjectDoesNotExist:
            return HttpResponseForbidden()

    def post(self, request: HttpRequest, uuid: str, id: int = None):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_admin():
            return HttpResponseForbidden()
        if id is None:
            counter_group = CounterGroup(counter_dashboard=user.counter_dashboard)
        else:
            try:
                counter_group = user.counter_dashboard.countergroup_set.get(id=id)
            except ObjectDoesNotExist:
                return HttpResponseForbidden()
        request_data = json.loads(request.body)
        if request_data.get('name', None):
            counter_group.name = request_data.get('name')
        counter_group.save()
        return JsonResponse(counter_group.as_dict())

    def delete(self, request: HttpRequest, uuid: str, id: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_admin():
            return HttpResponseForbidden()
        try:
            counter_group = user.counter_dashboard.countergroup_set.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        counter_group.delete()
        return HttpResponse(status=204)


@method_decorator(csrf_exempt, name='dispatch')
class ApiCounter(View):
    def get(self, request: HttpRequest, uuid: str, id: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        try:
            counter = Counter.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if counter.counter_group.counter_dashboard.id != user.counter_dashboard.id:
            return HttpResponseForbidden()
        return JsonResponse(counter.as_dict())

    def post(self, request: HttpRequest, uuid: str, id: int = None):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_admin():
            return HttpResponseForbidden()
        try:
            counter = Counter.objects.get(id=id)
        except ObjectDoesNotExist:
            counter = None
        if counter:
            if counter.counter_group.counter_dashboard.id != user.counter_dashboard.id:
                return HttpResponseForbidden()
        request_data = json.loads(request.body)
        if counter is None:  # Create a new counter
            try:
                if request_data.get('counterGroup', None):
                    counter_group = CounterGroup.objects.get(id=request_data.get('counterGroup'))
                    if counter_group.counter_dashboard.id != user.counter_dashboard.id:
                        return HttpResponseForbidden()
                    counter = Counter(counter_group=counter_group)
            except ObjectDoesNotExist:
                return HttpResponseForbidden()
        if counter is None:
            return HttpResponseForbidden()
        if request_data.get('name', None):
            counter.name = request_data.get('name')
        if request_data.get('order', None):
            counter.order = request_data.get('order')
        if request_data.get('value', None) is not None:
            counter.value = request_data.get('value')
        if request_data.get('deltaValue', None):
            counter.delta_value = request_data.get('deltaValue')
        if request_data.get('color', None):
            counter.color = request_data.get('color')
        counter.save()

        return JsonResponse(counter.as_dict())

    def delete(self, request: HttpRequest, uuid: str, id: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_admin():
            return HttpResponseForbidden()
        try:
            counter = Counter.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if counter.counter_group.counter_dashboard.id != user.counter_dashboard.id:
            return HttpResponseForbidden()
        counter.delete()
        return HttpResponse(status=204)


@method_decorator(csrf_exempt, name='dispatch')
class ApiCounterTrigger(View):
    def post(self, request: HttpRequest, uuid: str, id: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        try:
            counter = Counter.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if not user.is_counter_worker() or \
                counter.counter_group.counter_dashboard.id != user.counter_dashboard.id:
            return HttpResponseForbidden()
        counter.trigger(user)
        return JsonResponse(counter.as_dict())


@method_decorator(csrf_exempt, name='dispatch')
class ApiEventsPage(View):
    def get(self, request: HttpRequest, uuid: str, id: int, page: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        try:
            counter = Counter.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if counter.counter_group.counter_dashboard.id != user.counter_dashboard.id:
            return HttpResponseForbidden()
        events = counter.event_set.order_by("-timestamp")
        paginator = Paginator(events, 50)
        page_obj = paginator.get_page(page)
        page_obj = {
            'events': list(e.as_dict() for e in page_obj),
            'page': page,
            'numPages': paginator.num_pages
        }
        return JsonResponse(page_obj)


@method_decorator(csrf_exempt, name='dispatch')
class ApiEventsYear(View):
    def get(self, request: HttpRequest, uuid: str, id: int, year: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        try:
            counter = Counter.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if counter.counter_group.counter_dashboard.id != user.counter_dashboard.id:
            return HttpResponseForbidden()
        events = counter.event_set.filter(timestamp__year=year).order_by("timestamp")
        page_obj = {
            'events': list(e.as_dict() for e in events),
            'year': year
        }
        return JsonResponse(page_obj)


@method_decorator(csrf_exempt, name='dispatch')
class ApiCounterGroupEventsYear(View):
    def get(self, request: HttpRequest, uuid: str, id: int, year: int):
        try:
            user = CounterUser.get_user_by_uuid(uuid)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        try:
            counter_group = CounterGroup.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden()
        if counter_group.counter_dashboard.id != user.counter_dashboard.id:
            return HttpResponseForbidden()

        counter_ids = [c.id for c in counter_group.counter_set.all()]
        events = Event.objects.filter(counter__in=counter_ids, timestamp__year=year).order_by("timestamp")

        page_obj = {
            'events': list(e.as_dict(add_counter_id=True) for e in events),
            'year': year
        }
        return JsonResponse(page_obj)
