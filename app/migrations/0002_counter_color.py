# Generated by Django 4.2.2 on 2023-08-20 16:35

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='counter',
            name='color',
            field=colorfield.fields.ColorField(default='#9a9595', image_field=None, max_length=18, samples=None),
        ),
    ]
