from django import forms


class CreateDashboardForm(forms.Form):
    name = forms.CharField(label='Dashboard name', max_length=256)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
