from django.contrib import admin

# Register your models here.
from app.models import CounterDashboard, CounterAdmin, CounterWorker, CounterViewer, CounterGroup, Counter, Event


@admin.register(CounterDashboard)
class CounterDashboardAdmin(admin.ModelAdmin):
    list_display = ('name', 'created')


@admin.register(CounterAdmin)
class CounterAdminAdmin(admin.ModelAdmin):
    list_display = ('name', 'uuid')


@admin.register(CounterWorker)
class CounterWorkerAdmin(admin.ModelAdmin):
    list_display = ('name', 'uuid')


@admin.register(CounterViewer)
class CounterViewerAdmin(admin.ModelAdmin):
    list_display = ('name', 'uuid')


@admin.register(CounterGroup)
class CounterGroupAdmin(admin.ModelAdmin):
    pass


@admin.register(Counter)
class CounterAdmin(admin.ModelAdmin):
    pass


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    pass
