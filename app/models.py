import uuid
from abc import abstractmethod
from builtins import super

import channels.layers
from asgiref.sync import async_to_sync
from colorfield.fields import ColorField
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction
from django.db.models import Model


class CounterDashboard(models.Model):
    name = models.CharField(max_length=256, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)

    def as_dict(self, add_user_infos=False):
        result = {
            'id': self.id,
            'name': self.name,
            'created': self.created,
            'counterGroups': [counter_group.as_dict() for counter_group in self.countergroup_set.all()]
        }
        if add_user_infos:
            result['admins'] = [user.as_dict() for user in self.counteradmin_set.all()]
            result['workers'] = [user.as_dict() for user in self.counterworker_set.all()]
            result['viewers'] = [user.as_dict() for user in self.counterviewer_set.all()]

        return result

    def save(self, *args, **kwargs):
        new = self._state.adding
        super().save(*args, **kwargs)
        if new:
            with transaction.atomic():
                CounterAdmin(name="Default Admin", counter_dashboard=self).save()
                CounterWorker(name="Default Worker", counter_dashboard=self).save()
                CounterViewer(name="Default Viewer", counter_dashboard=self).save()

                new_counter_group = CounterGroup(name="Default Counter Group", counter_dashboard=self)
                new_counter_group.save()
                Counter(name="Counter 1", counter_group=new_counter_group).save()
                Counter(name="Counter 2", counter_group=new_counter_group).save()


class CounterUser(models.Model):
    counter_dashboard = models.ForeignKey(CounterDashboard, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, null=True, blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True, unique=True)

    class Meta:
        abstract = True

    def as_dict(self):
        return {
            'uuid': self.uuid,
            'name': self.name,
        }

    @abstractmethod
    def is_counter_admin(self):
        raise NotImplementedError

    @abstractmethod
    def is_counter_worker(self):
        raise NotImplementedError

    @abstractmethod
    def is_counter_viewer(self):
        raise NotImplementedError

    @staticmethod
    def get_user_by_uuid(user_uuid: str) -> 'CounterUser':
        for subclass in CounterUser.__subclasses__():
            try:
                return subclass.objects.get(uuid=user_uuid)
            except ObjectDoesNotExist:
                pass
        raise ObjectDoesNotExist(f"No user found with uuid {user_uuid}.")


class CounterAdmin(CounterUser):

    def is_counter_admin(self):
        return True

    def is_counter_worker(self):
        return True

    def is_counter_viewer(self):
        return True


class CounterWorker(CounterUser):
    def is_counter_admin(self):
        return False

    def is_counter_worker(self):
        return True

    def is_counter_viewer(self):
        return True


class CounterViewer(CounterUser):
    def is_counter_admin(self):
        return False

    def is_counter_worker(self):
        return False

    def is_counter_viewer(self):
        return True


class CounterGroup(models.Model):
    counter_dashboard = models.ForeignKey(CounterDashboard, on_delete=models.CASCADE)
    order = models.FloatField(default=1.0)
    name = models.CharField(max_length=256, null=True, blank=True)

    # TODO: style
    # TODO: order

    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'counter': [counter.as_dict() for counter in self.counter_set.all().order_by("order")]
        }


class Counter(models.Model):
    counter_group = models.ForeignKey(CounterGroup, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, null=True, blank=True)
    order = models.FloatField(default=1.0)
    value = models.FloatField(default=0.0)
    delta_value = models.FloatField(default=1.0)
    updated = models.DateTimeField(auto_now_add=True, blank=True)
    color = ColorField(default='#9a9595')

    # TODO: font color
    # TODO: bg color
    # TODO: style
    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'order': self.order,
            'value': round(self.value, 2),
            'deltaValue': round(self.delta_value, 2),
            'updated': self.updated.isoformat(),
            'counterGroup': self.counter_group.id,
            'color': self.color
        }

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super().from_db(db, field_names, values)
        # save original values, when model is loaded from database, in a separate attribute on the model
        instance._old_value = dict(zip(field_names, values))['value']
        return instance

    def trigger(self, user: CounterUser):
        assert user.counter_dashboard_id == self.counter_group.counter_dashboard_id
        assert user.is_counter_worker()
        old_counter_value = self.value
        self.value += self.delta_value
        self.updated = timezone.now()
        self.save()

    def save(self, *args, **kwargs):
        new = self._state.adding
        if new:
            all_other_counters = self.counter_group.counter_set.all()
            self.order = 1 + max(c.order for c in all_other_counters) if len(all_other_counters) > 0 else 1
        else:
            if self._old_value != self.value:
                Event(counter=self, old_value=self._old_value, new_value=self.value).save()
        super().save(*args, **kwargs)
        # Inform clients
        channel_layer = channels.layers.get_channel_layer()
        group_name = f'dbg-{self.counter_group.counter_dashboard_id}'
        async_to_sync(channel_layer.group_send)(
            group_name,
            {
                "type": "update_counter",
                "counterData": self.as_dict(),
            }
        )

    def delete(self, using=None, keep_parents=False):
        channel_layer = channels.layers.get_channel_layer()
        group_name = f'dbg-{self.counter_group.counter_dashboard_id}'
        async_to_sync(channel_layer.group_send)(
            group_name,
            {
                "type": "delete_counter",
                "counterId": self.id,
            }
        )
        super().delete(using, keep_parents)


class Event(models.Model):
    counter = models.ForeignKey(Counter, on_delete=models.CASCADE)
    old_value = models.FloatField()
    new_value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)

    def as_dict(self, add_counter=False, add_counter_id=False):
        return {
            'id': self.id,
            'old_value': self.old_value,
            'new_value': self.new_value,
            'timestamp': self.timestamp.isoformat(),
            **({'counter': self.counter.as_dict()} if add_counter else {}),
            **({'counter_id': self.counter_id} if add_counter_id else {}),
        }
