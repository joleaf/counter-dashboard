import json
import logging

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from django.core.exceptions import ObjectDoesNotExist

from app.models import CounterUser

logger = logging.getLogger(__name__)


class DashboardConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        room_id = int(self.scope['url_route']['kwargs']['room'])
        self.group_name = f"dbg-{room_id}"
        user_id = self.scope['url_route']['kwargs']['uuid']
        try:
            user = await sync_to_async(CounterUser.get_user_by_uuid)(user_id)
            if user.counter_dashboard_id != room_id:
                raise ObjectDoesNotExist
        except ObjectDoesNotExist:
            await self.accept()
            await self.close(4004)
            return

        # Join room group
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        logger.info("LOL")
        pass

    async def update_counter(self, event):
        await self.send(text_data=json.dumps(event))

    async def delete_counter(self, event):
        await self.send(text_data=json.dumps(event))
