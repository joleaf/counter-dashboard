const apiUrl = window.location.origin + "/api/dashboard/";
const apiUrlDashboard = apiUrl + userUuid + "/";
const apiUrlCounterGroup = apiUrl + userUuid + "/countergroup/";
const apiUrlCounter = apiUrl + userUuid + "/counter/";
const apiUrlCounterEventsPage = apiUrl + userUuid + "/counter/$counter_id/events/page/$page/";
const apiUrlCounterEventsYear = apiUrl + userUuid + "/counter/$counter_id/events/year/$year/";
const apiUrlCounterGroupEventsYear = apiUrl + userUuid + "/countergroup/$counter_group_id/events/year/$year/";
let dragSrcEl = null;
let dragCounter = 0;
let dashboardId = null;
let tooltip = null;

function handleDragStart(e) {
    this.style.opacity = '0.2';
    dragSrcEl = this;
    dragCounter = 0;
    e.originalEvent.dataTransfer.dropEffect = 'move';
    e.originalEvent.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragEnd(e) {
    this.style.opacity = '1';
    let $allCounterCards = $('.counter-card');
    $allCounterCards.each(function () {
        this.classList.remove('border');
        this.classList.remove('border-secondary');
    })
}

function handleDragOver(e) {
    e.preventDefault();
    return false;
}

function handleDragEnter(e) {
    dragCounter++;
    this.classList.add('border');
    this.classList.add('border-secondary');
}

function handleDragLeave(e) {
    dragCounter--;
    //console.log(dragCounter);
    if (dragCounter === 0) {
        this.classList.remove('border');
        this.classList.remove('border-secondary');
    }
}

function handleDrop(e) {
    e.stopPropagation();
    if (dragSrcEl !== this) {
        dragSrcEl.innerHTML = this.innerHTML;
        this.innerHTML = e.originalEvent.dataTransfer.getData('text/html');
        //console.log(dragSrcEl.getAttribute('data-counter-id'));
        //console.log(dragSrcEl.getAttribute('data-counter-order'));
        //console.log(this.getAttribute('data-counter-id'));
        //console.log(this.getAttribute('data-counter-order'));
        let oldOrder = this.getAttribute('data-counter-order');
        let oldOtherOrder = dragSrcEl.getAttribute('data-counter-order');
        updateCounterOrder(
            dragSrcEl.getAttribute('data-counter-id'),
            oldOrder
        );
        updateCounterOrder(
            this.getAttribute('data-counter-id'),
            oldOtherOrder
        );
        addAlertInfoMessage("Counter position moved!");
    }
    return false;
}

function buildCounter(counterData) {
    const deltaSign = counterData.deltaValue >= 0 ? "+" : "";
    let editDeleteButtons = userIsAdmin ? `
                      <div class="float-start">            
                          <a class="btn btn-secondary btn-sm" role="button" aria-pressed="true" onclick="editCounterValues(${counterData.id})">
                            <p class="card-text" id="counter_card_edit_${counterData.id}">Edit</p>
                          </a>
                          <a class="btn btn-danger btn-sm" role="button" aria-pressed="true" onclick="deleteCounterModal(${counterData.id})">
                             <p class="card-text" id="counter_card_delete_${counterData.id}">Delete</p>
                          </a>
                          <button class="btn btn-primary btn-sm" role="button" aria-pressed="true" onclick="copyToClipboard('${getApiUrlCounterTriggerUrl(counterData.id)}','Counter trigger API url copied to clipboard. Note that you need a POST request.')">
                             Copy Trigger API Url
                          </button>
                      </div>` : '';
    let showEventButton = `<div class="mx-3 float-end">            
                          <button class="btn btn-primary btn-sm" role="button" aria-pressed="true" onclick="showEvents('${counterData.id}')">
                             Show events
                          </button>
                      </div>`;

    let triggerButton = userIsWorker ? `
                      <div class="float-end">            
                          <a class="btn btn-primary" role="button" aria-pressed="true" onclick="triggerCounter(${counterData.id})">
                              <p class="card-text fs-3 align-bottom" style="width: 90px;" id="counter_card_delta_value_${counterData.id}">${deltaSign}${counterData.deltaValue}</p>
                          </a>
                      </div>` : '';

    let newCounterCard = $(`
          <div class="card mb-2 counter-card" 
            id="counter_card_${counterData.id}" 
            data-counter-id="${counterData.id}"
            data-counter-order="${counterData.order}">
              <div class="card-header">
                  <div class="d-grid mx-auto">  
                     <h5 class="card-title" id="counter_card_name_${counterData.id}">${counterData.name}</h5>
                     <h6 class="card-subtitle mb-2 text-muted">
                         Last triggered: <span id="counter_card_updated_${counterData.id}">${formatDate(counterData.updated)}</span> 
                         <!--(<span id="counter_card_updateddiff_${counterData.id}">${formatDatediff(counterData.updated)}</span>)-->
                     </h6>
                  </div>
                  ${editDeleteButtons}
                  ${showEventButton}
              </div>
              <div class="card-body pb-1">
                  <p class="col-8 card-text fs-1 fw-bold float-start" style="color: ${counterData.color};" id="counter_card_value_${counterData.id}">${counterData.value}</p>
                  ${triggerButton}
              </div>
        </div>`);
    if (userIsAdmin) {
        newCounterCard.attr('draggable', 'true');
        newCounterCard.on('dragstart', handleDragStart);
        newCounterCard.on('dragenter', handleDragEnter);
        newCounterCard.on('dragleave', handleDragLeave);
        newCounterCard.on('dragover', handleDragOver);
        newCounterCard.on('dragend', handleDragEnd);
        newCounterCard.on('drop', handleDrop);
    }
    return newCounterCard;
}

function triggerCounter(counter_id) {
    const apiUrlCounterTrigger = getApiUrlCounterTriggerUrl(counter_id);
    $.ajax({
        url: apiUrlCounterTrigger,
        type: "POST",
        success: updateCounterCard,
    })
}

function getApiUrlCounterTriggerUrl(counter_id) {
    return apiUrl + userUuid + "/counter/" + counter_id + "/trigger/";
}

function showEvents(counter_id) {
    $.ajax({
        url: apiUrlCounterEventsPage.replace("$counter_id", counter_id).replace("$page", 1),
        type: "GET",
        success: function (data) {
            showEventsModal(counter_id, data);
        }
    });
}

function showEventsModal(counter_id, data) {
    let myEventsModal = $(`
        <div class="modal" id="counterEventsModal" role="dialog">
            <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="counterEventsModalLongTitle">Events</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                   </div>
                   <div class="modal-body" id="event-data-list">
                       <div class="accordion" id="accordionCalendar">
                           <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#accordionCalendarEl" aria-expanded="true" aria-controls="urlList">
                                        Calendar
                                    </button>
                                </h3>
                                <div id="accordionCalendarEl" class="accordion-collapse collapse"
                                     data-bs-parent="#accordionCalendar">
                                    <div id="eventCalendar" class="accordion-body">
                                    
                                    </div>
                                </div>
                           </div>
                       </div>
                       <h3>Events</h3>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                   </div>
                </div>
            </div>
        </div>
    `);
    $('#dashboard-container').append(myEventsModal);
    let myModal = new bootstrap.Modal(document.getElementById('counterEventsModal'), {})
    myModal.show();
    $('#counterEventsModal').on('hidden.bs.modal', function (event) {
        $('#counterEventsModal').remove();
    });
    let listView = $('#event-data-list');
    let listGroup = listView.append("<ul class='list-group'></ul>");
    let currentPage = 1;
    let maxPages = data.numPages;
    addItemsFromPage(data);
    listView.on("scroll", function (event) {
        let viewEl = event.currentTarget;
        if (viewEl.offsetHeight + viewEl.scrollTop + 0.5 >= viewEl.scrollHeight) {
            if (currentPage < maxPages) {
                $.ajax({
                    url: apiUrlCounterEventsPage.replace("$counter_id", counter_id).replace("$page", currentPage + 1),
                    type: "GET",
                    success: addItemsFromPage,
                });
            }
        }
    });

    function addItemsFromPage(data) {
        currentPage = data.page;
        for (const event of data.events) {
            let delta = event.new_value - event.old_value;
            if (delta > 0) {
                delta = "+" + delta;
            }
            listGroup.append(`<li class='list-group-item'>
                <span class="badge bg-secondary">
                    ${event.old_value} &#8594; ${event.new_value} (${delta})
                </span>
                <br/>
                <span>
                    on ${formatDate(event.timestamp)} (${formatDatediff(event.timestamp)} ago)
                </span>
                </li>`);
        }
    }

    // Load the calendar for this counter
    new Calendar(
        document.querySelector('#eventCalendar'),
        {
            maxDate: new Date(),
            dataSource: function ({year}) {
                // Load data from GitHub API
                return fetch(apiUrlCounterEventsYear.replace("$counter_id", counter_id).replace("$year", year))
                    .then(result => result.json())
                    .then(result => {
                        if (result.events) {
                            return result.events.map(r => ({
                                startDate: new Date(r.timestamp),
                                endDate: new Date(r.timestamp),
                                name: `${r.old_value} &#8594; ${r.new_value} (${formatTime(r.timestamp)})`,
                                color: $(`#counter_card_value_${counter_id}`)[0].style.color
                            }));
                        }
                        return [];
                    });
            },
            mouseOnDay: function (e) {
                if (e.events.length > 0) {
                    let content = $(`<div class="row row-cols-2"></div>`);
                    for (let i in e.events) {
                        content.append($(`<div class="col event-tooltip-content">
                                            <div class="event-name" style="white-space: nowrap; color: ${e.events[i].color}">${e.events[i].name}</div>
                                         </div>`));
                    }
                    if (tooltip !== null) {
                        tooltip.destroy();
                    }
                    tooltip = tippy(e.element, {
                        placement: 'right',
                        content: content[0],
                        animateFill: false,
                        animation: 'shift-away',
                        arrow: true
                    });
                    tooltip.show();
                }
            },
            mouseOutDay: function () {
                if (tooltip !== null) {
                    tooltip.destroy();
                    tooltip = null;
                }
            }
        }
    );
}


function copyToClipboard(data, message) {
    navigator.clipboard.writeText(data);
    addAlertInfoMessage(message);
}

function updateCounterCard(counterData) {
    const deltaSign = counterData.deltaValue >= 0 ? "+" : "";
    $(`#counter_card_${counterData.id}`).attr("data-counter-order", counterData.order);
    $(`#counter_card_value_${counterData.id}`).text(counterData.value);
    $(`#counter_card_delta_value_${counterData.id}`).text(deltaSign + counterData.deltaValue);
    $(`#counter_card_updated_${counterData.id}`).text(formatDate(counterData.updated));
    // $(`#counter_card_updateddiff_${counterData.id}`).text(formatDatediff(counterData.updated));
    $(`#counter_card_name_${counterData.id}`).text(counterData.name);
    $(`#counter_card_value_${counterData.id}`)[0].style.color = counterData.color;
}

function updatedCounter(counterData) {
    let $counter = $('#counter_card_' + counterData.id);
    if ($counter.length === 0) {
        let $counterGroup = $('#countergroup_counter_' + counterData.counterGroup);
        let $newCounter = buildCounter(counterData);
        $counterGroup.append($newCounter);
        $newCounter[0].scrollIntoView({
            behavior: "smooth", // or "auto" or "instant"
            block: "start" // or "end"
        });
        addAlertInfoMessage("Added a new counter!");
    } else {
        updateCounterCard(counterData);
    }
}

function editCounterValues(counterId) {
    $.ajax({
        url: apiUrlCounter + counterId + "/",
        type: "GET",
        success: function (data) {
            editCounterModal(data)
        }
    });
}

function editCounterModal(counterData) {
    let myEditModal = $(`
        <div class="modal" id="editCounterModal" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editCounterModalLongTitle">Edit Counter</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                   </div>
                   <form id="myEditForm">
                       <input type="hidden" name="counterId" value="${counterData.id}">
                       <div class="modal-body">
                          <div class="mb-3">
                            <label for="counterName" class="form-label">Counter Name</label>
                            <input type="text" class="form-control" id="counterName" name="counterName" value="${counterData.name}">
                          </div>
                          <div class="mb-3">
                            <label for="counterValue" class="form-label">Current Value</label>
                            <input type="number" class="form-control" id="counterValue" name="counterValue" value="${counterData.value}">
                          </div>
                          <div class="mb-3">
                            <label for="counterDeltaValue" class="form-label">Delta Value</label>
                            <input type="number" class="form-control" id="counterDeltaValue" name="counterDeltaValue" value="${counterData.deltaValue}">
                          </div>
                          <div class="mb-3">
                            <label for="counterColor" class="form-label">Color</label>
                            <input type="text" style="background-color: ${counterData.color};" class="form-control" id="counterColor" name="counterColor" value="${counterData.color}" data-coloris>
                          </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                           <input type="submit" class="btn btn-primary" value="Save">
                       </div>
                   </form>
                </div>
            </div>
        </div>
    `);
    $('#dashboard-container').append(myEditModal);
    let myModal = new bootstrap.Modal(document.getElementById('editCounterModal'), {})
    myModal.show();
    $('#editCounterModal').on('hidden.bs.modal', function (event) {
        $('#editCounterModal').remove();
    })
    let colorField = $('#counterColor');
    colorField.on('input', function (e) {
        colorField[0].style.backgroundColor = colorField[0].value;
    });
    $('#myEditForm').on("submit", function (e) {
        e.preventDefault(); // cancel the actual submit
        let data = $("#myEditForm").serializeArray();
        editCounter(data.find(e => e.name === 'counterId').value,
            data.find(e => e.name === 'counterName').value,
            data.find(e => e.name === 'counterValue').value,
            data.find(e => e.name === 'counterDeltaValue').value,
            data.find(e => e.name === 'counterColor').value
        );
    });
}

function editCounter(counterId, counterName, counterValue, counterDeltaValue, counterColor) {
    $.ajax({
        url: apiUrlCounter + counterId + "/",
        type: "POST",
        data: JSON.stringify({
                name: counterName,
                value: parseFloat(counterValue),
                deltaValue: parseFloat(counterDeltaValue),
                color: counterColor
            }
        ),
        success: function (data) {
            editCounterSuccess(data)
        }
    });
}

function editCounterSuccess(counterData) {
    var myModal = bootstrap.Modal.getInstance(document.getElementById('editCounterModal'))
    updatedCounter(counterData);
    myModal.hide();
    addAlertInfoMessage("Counter updated!");
}

function deleteCounterModal(counterId) {
    let myConfirmModal = $(`
        <div class="modal" id="deleteCounterModal" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteCounterModalLongTitle">Confirm</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                   </div>
                   <div class="modal-body">
                        Are you sure to delete this <strong>Counter</strong>?
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                       <input type="button" class="btn btn-danger" value="Delete" onclick="deleteCounter(${counterId})">
                   </div>
                </div>
            </div>
        </div>
    `);
    $('#dashboard-container').append(myConfirmModal);
    var myModal = new bootstrap.Modal(document.getElementById('deleteCounterModal'), {})
    myModal.show();
    $('#deleteCounterModal').on('hidden.bs.modal', function (event) {
        $('#deleteCounterModal').remove();
    })
}

function deleteCounter(counterId) {
    $.ajax({
        url: apiUrlCounter + counterId + "/",
        type: "DELETE",
        success: function (data) {
            deleteCounterSuccess(counterId)
        }
    });
}

function deleteCounterSuccess(counterId) {
    var myModal = bootstrap.Modal.getInstance(document.getElementById('deleteCounterModal'))
    $('#counter_card_' + counterId).remove();
    myModal.hide();
    addAlertInfoMessage("Counter deleted!");
}

function buildCounterGroup(counterGroupData) {
    let editDeleteButtons = userIsAdmin ? `
                    <div class="float-start ">            
                        <a class="btn btn-secondary btn-sm" role="button" aria-pressed="true" onclick="editCounterGroupValues(${counterGroupData.id})">
                            <p class="card-text" id="countergroup_card_edit_${counterGroupData.id}">Edit</p>
                        </a>
                        <a class="btn btn-danger btn-sm" role="button" aria-pressed="true" onclick="deleteCounterGroupModal(${counterGroupData.id})">
                            <p class="card-text" id="countergroup_card_delete_${counterGroupData.id}">Delete</p>
                        </a>
                    </div>` : '';
    let showCalendarCounterGroupButton = `<div class="mx-3 float-end">            
                          <button class="btn btn-primary btn-sm" role="button" aria-pressed="true" onclick="showCalendarCounterGroupModal('${counterGroupData.id}')">
                             Show calendar
                          </button>
                      </div>`;
    let counterGroupContainer = $(`
        <div class="card mt-3" id="countergroup_${counterGroupData.id}">
            <div class="card-header">
                <div class="d-grid mx-auto"> 
                     <h4 class="card-title" id="counter_group_card_name_${counterGroupData.id}">${counterGroupData.name}</h4> 
                </div>
                ${editDeleteButtons}
                ${showCalendarCounterGroupButton}
            </div>
        </div>`);

    let counterDiv = $(`<div class="card-body card-columns" id="countergroup_counter_${counterGroupData.id}"></div>`)
    counterGroupContainer.append(counterDiv);
    for (const counter of counterGroupData.counter) {
        counterDiv.append(buildCounter(counter));
    }
    if (userIsAdmin) {
        counterGroupContainer.append(`
        <button class="btn btn-primary float-end" onclick="createCounter(${counterGroupData.id})">Add a new counter</button>
    `)
    }
    return counterGroupContainer;
}

function updateCounterGroupCard(counterGroupData) {
    $(`#counter_group_card_name_${counterGroupData.id}`).text(counterGroupData.name)
}

function updatedCounterGroup(counterGroupData) {
    let $counterGroup = $('#countergroup_' + counterGroupData.id);
    if ($counterGroup.length === 0) {
        let $dashboardContainer = $('#dashboard-container');
        let $newCounterGroup = buildCounterGroup(counterGroupData);
        $dashboardContainer.append($newCounterGroup);
        $newCounterGroup[0].scrollIntoView({
            behavior: "smooth", // or "auto" or "instant"
            block: "start" // or "end"
        });
        addAlertInfoMessage("You successfully added a new counter group!");
    } else {
        updateCounterGroupCard(counterGroupData);
    }
}

function editCounterGroupValues(counterGroupId) {
    $.ajax({
        url: apiUrlCounterGroup + counterGroupId + "/",
        type: "GET",
        success: function (data) {
            editCounterGroupModal(data)
        }
    });
}

function editCounterGroupModal(counterGroupData) {
    let myEditModal = $(`
        <div class="modal" id="editCounterGroupModal" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editCounterGroupModalLongTitle">Edit Counter Group</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                   </div>
                   <form id="myEditForm">
                       <input type="hidden" name="counterGroupId" value="${counterGroupData.id}">
                       <div class="modal-body">
                          <div class="mb-3">
                            <label for="counterGroupName" class="form-label">Counter Name</label>
                            <input type="text" class="form-control" id="counterGroupName" name="counterGroupName" value="${counterGroupData.name}">
                          </div>
                          <!--
                          <div class="mb-3">
                            <label for="counterGroupOrder" class="form-label">Order</label>
                            <input type="number" class="form-control" id="counterGroupOrder" name="counterGroupOrder" value="${counterGroupData.order}">
                          </div>-->
                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                           <input type="submit" class="btn btn-primary" value="Save">
                       </div>
                   </form>
                </div>
            </div>
        </div>
    `);
    $('#dashboard-container').append(myEditModal);
    let myModal = new bootstrap.Modal(document.getElementById('editCounterGroupModal'), {})
    myModal.show();
    $('#editCounterGroupModal').on('hidden.bs.modal', function (event) {
        $('#editCounterGroupModal').remove();
    })
    $('#myEditForm').on("submit", function (e) {
        e.preventDefault(); // cancel the actual submit
        let data = $("#myEditForm").serializeArray();
        editCounterGroup(data.find(e => e.name === 'counterGroupId').value,
            data.find(e => e.name === 'counterGroupName').value,
            //data.find(e => e.name === 'counterGroupOrder').value,
        );
    });
}

function editCounterGroup(counterGroupId, counterGroupName, counterGroupOrder) {
    $.ajax({
        url: apiUrlCounterGroup + counterGroupId + "/",
        type: "POST",
        data: JSON.stringify({
                name: counterGroupName,
                //order: parseFloat(counterGroupOrder),
            }
        ),
        success: function (data) {
            editCounterGroupSuccess(data)
        }
    });
}

function editCounterGroupSuccess(counterGroupData) {
    let myModal = bootstrap.Modal.getInstance(document.getElementById('editCounterGroupModal'))
    updatedCounterGroup(counterGroupData);
    myModal.hide();
    addAlertInfoMessage("Counter Group updated!");
}

function deleteCounterGroupModal(counterGroupId) {
    let myConfirmModal = $(`
        <div class="modal" id="deleteCounterGroupModal" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteCounterGroupModalLongTitle">Confirm</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                   </div>
                   <div class="modal-body">
                        Are you sure to delete this <strong>Counter Group</strong>?
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                       <input type="button" class="btn btn-danger" value="Delete" onclick="deleteCounterGroup(${counterGroupId})">
                   </div>
                </div>
            </div>
        </div>
    `);
    $('#dashboard-container').append(myConfirmModal);
    let myModal = new bootstrap.Modal(document.getElementById('deleteCounterGroupModal'), {})
    myModal.show();
    $('#deleteCounterGroupModal').on('hidden.bs.modal', function (event) {
        $('#deleteCounterGroupModal').remove();
    })
}

function deleteCounterGroup(counterGroupId) {
    $.ajax({
        url: apiUrlCounterGroup + counterGroupId + "/",
        type: "DELETE",
        success: function (data) {
            deleteCounterGroupSuccess(counterGroupId)
        }
    });
}

function deleteCounterGroupSuccess(counterGroupId) {
    var myModal = bootstrap.Modal.getInstance(document.getElementById('deleteCounterGroupModal'))
    $('#countergroup_' + counterGroupId).remove();
    myModal.hide();
    addAlertInfoMessage("Counter group deleted!");
}

function showCalendarCounterGroupModal(counterGroupId) {
    let myCalendarCounterGroupModal = $(`
        <div class="modal" id="calendarCounterGroupModal" role="dialog">
            <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="calendarCounterGroupModalLongTitle">Counter Group Calendar</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                   </div>
                   <div class="modal-body" id="eventCalendar">
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                   </div>
                </div>
            </div>
        </div>
    `);
    $('#dashboard-container').append(myCalendarCounterGroupModal);
    let myModal = new bootstrap.Modal(document.getElementById('calendarCounterGroupModal'), {})
    myModal.show();
    $('#calendarCounterGroupModal').on('hidden.bs.modal', function (event) {
        $('#calendarCounterGroupModal').remove();
    });

    // Load the calendar for this counter
    new Calendar(
        document.querySelector('#eventCalendar'),
        {
            maxDate: new Date(),
            dataSource: function ({year}) {
                // Load data from GitHub API
                return fetch(apiUrlCounterGroupEventsYear.replace("$counter_group_id", counterGroupId).replace("$year", year))
                    .then(result => result.json())
                    .then(result => {
                        if (result.events) {
                            return result.events.map(r => ({
                                startDate: new Date(r.timestamp),
                                endDate: new Date(r.timestamp),
                                name: `${r.old_value} &#8594; ${r.new_value} (${formatTime(r.timestamp)})`,
                                color: $(`#counter_card_value_${r.counter_id}`)[0].style.color
                            }));
                        }
                        return [];
                    });
            },
            mouseOnDay: function (e) {
                if (e.events.length > 0) {
                    let content = $(`<div class="row row-cols-2"></div>`);
                    for (let i in e.events) {
                        content.append($(`<div class="col event-tooltip-content">
                                            <div class="event-name" style="white-space: nowrap; color: ${e.events[i].color}">${e.events[i].name}</div>
                                         </div>`));
                    }
                    if (tooltip !== null) {
                        tooltip.destroy();
                    }
                    tooltip = tippy(e.element, {
                        placement: 'right',
                        content: content[0],
                        animateFill: false,
                        animation: 'shift-away',
                        arrow: true
                    });
                    tooltip.show();
                }
            },
            mouseOutDay: function () {
                if (tooltip !== null) {
                    tooltip.destroy();
                    tooltip = null;
                }
            }
        }
    );
}

function buildDashboard(dashboardData) {
    dashboardId = dashboardData.id;
    registerWebsocket();
    let dashboardContainer = $('#dashboard-container');
    $('#dashboard_created').text(formatDate(dashboardData.created));
    dashboardContainer.empty();
    if (userIsAdmin) {
        dashboardContainer.append(`
            <div class="mb-2">
                <button class="btn btn-primary" onclick="createCounterGroup()">Add a new counter group</button>
            </div>
        `);
    }
    for (const counterGroup of dashboardData.counterGroups) {
        dashboardContainer.append(buildCounterGroup(counterGroup));
    }
}

function registerWebsocket() {
    // Configure Websocket
    try {
        let wsProtocol = window.location.protocol === "https:" ? 'wss://' : 'ws://';
        let webSocketUrl = wsProtocol
            + window.location.host
            + '/ws/dashboard/' + dashboardId
            + '/' + userUuid;
        const chatSocket = new WebSocket(
            webSocketUrl
        );
        chatSocket.onmessage = wsOnMessage;
        chatSocket.onclose = function (e) {
            console.error('Chat socket closed unexpectedly');
        };
    } catch (e) {
        console.error("Error WS")
        console.error(e)
    }
}


function createCounterGroup() {
    $.ajax({
        url: apiUrlCounterGroup,
        type: "POST",
        data: JSON.stringify({"name": "New Counter Group"}),
        success: updatedCounterGroup,
    });
}

function createCounter(counter_group_id) {
    $.ajax({
        url: apiUrlCounter,
        type: "POST",
        data: JSON.stringify({
                name: "New Counter",
                counterGroup: counter_group_id,
                value: 0.0,
                deltaValue: 1.0
            }
        ),
        success: updatedCounter,
    });
}

function updateCounterOrder(counterId, newCounterOrder) {
    $.ajax({
        url: apiUrlCounter + counterId + "/",
        type: "POST",
        data: JSON.stringify({
                order: newCounterOrder,
            }
        ),
        success: updatedCounter,
    });
}


// WEB SOCKET FUNCTIONS
function wsOnMessage(e) {
    let data = JSON.parse(e.data);
    if (data.type === "update_counter") {
        wsUpdateCounter(data);
    } else if (data.type === "delete_counter") {
        wsDeleteCounter(data);
    }
}

function wsUpdateCounter(data) {
    updatedCounter(data.counterData);
}

function wsDeleteCounter(data) {
    deleteCounterSuccess(data.counterId);
}

// A $( document ).ready() block.
$(document).ready(function () {
    $.ajax({
        url: apiUrlDashboard,
        success: buildDashboard,
    });


});

function addAlertInfoMessage(msg) {
    $newAlert = $(`
        <div class="alert alert-primary alert-dismissible fade show" role="alert" data-tor="show:[rotateX.from(90deg) @--tor-translateZ(-5rem; 0rem) pull.down(full)] slow">
          <strong>Info!</strong> ${msg}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    `)
    $('#alerts').append($newAlert);
    $newAlert.fadeTo(3000, 1000).slideUp(1000, function () {
    });
}

function formatDate(date) {
    let d = dayjs(date);
    return d.format("YYYY-MM-DD HH:mm:ss");
}

function formatTime(date) {
    let d = dayjs(date);
    return d.format("HH:mm:ss");
}

function formatDatediff(date) {
    let d = dayjs(date);
    let now = dayjs();

    let diffYears = now.diff(d, 'years');
    if (diffYears > 1) {
        return diffYears + " years";
    }

    let diffMonths = now.diff(d, 'months');
    if (diffMonths > 1) {
        return diffMonths + " months";
    }

    let diffDays = now.diff(d, 'days');
    if (diffDays > 1) {
        return diffDays + " days";
    }
    if (diffDays === 1) {
        return diffDays + " day";
    }

    let diffHours = now.diff(d, 'hours');
    if (diffHours > 1) {
        return diffHours + " hours";
    }
    if (diffHours === 1) {
        return diffHours + " hour";
    }

    let diffMinutes = now.diff(d, 'minutes');
    if (diffMinutes > 1) {
        return diffMinutes + " minutes";
    }
    if (diffMinutes === 1) {
        return diffMinutes + " minute";
    }

    let doffSeconds = now.diff(d, 'seconds');
    if (doffSeconds === 1) {
        return doffSeconds + " second";
    }
    return doffSeconds + " seconds";
}