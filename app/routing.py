
from django.urls import path

from .consumers import DashboardConsumer

websocket_urlpatterns = [
    path('ws/dashboard/<int:room>/<uuid:uuid>', DashboardConsumer.as_asgi()),
]
