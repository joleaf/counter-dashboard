[![pipeline status](https://gitlab.com/joleaf/counter-dashboard/badges/main/pipeline.svg)](https://gitlab.com/joleaf/counter-dashboard/-/pipelines/latest)
[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/joleaf/counter-dashboard?label=Docker%20Image)](https://hub.docker.com/r/joleaf/counter-dashboard)

# Counter Dashboard

*Count everything with this **Counter Dashboard**!*

- Habits
- Scores
- Sports
- Meditations
- ...

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/C0C5GBZXQ)

## Features

- Create your own *Dashboard* for your *Counters*
- Use *Counter Groups* to group your *Counters*
- Highlight your *Counters* with different colors
- Choose a counter value (default: +1)
- View the history:
    - Events for counter
    - Calendar overview (e.g., for habit tracking)
- Use the trigger API for external hooks
- Three user types:
    - Admins: create & update *Counters*, manages the *Dashboard*
    - Workers: trigger *Counter*
    - Viewer: just view the current state of the *Dashboard*

## Start the Counter Dashboard

### Development

#### 1. Use docker compose to start the server

Use the [docker-compose-dev.yml](./docker-compose-dev.yml) for the local development.

```shell
docker compose -f docker-compose-dev.yml up --build
```

#### 2. Test the app

visit [http://localhost:8000](http://localhost:8000)

#### 3. Make changes

Python files will automatically be updated, js/css files must be collected:

```shell
python manage.py collectstatic --noinput
```

### Production

Use the [docker-compose.yml](./docker-compose.yml)

```shell
docker compose up -d
```

## User Login

If enabled, login with the provided user credentials from the environment `DASHBOARD_USER` & `DASHBOARD_USER_PASSWORD`.
Disable the login for the Counter Dashboard with `REQUIRE_LOGIN: false`.
The user will always be created, make sure to select a proper password!

This user can also log in into the Django Admin backend: [http://localhost:8000/admin](http://localhost:8000/admin)

## Future Features

- [ ] User management
- [ ] More visual statistics

## Technology Stack

- [Django](https://www.djangoproject.com/)
- [Redis](https://redis.io/)
- [MariaDB](https://mariadb.org/)
- [Docker](https://www.docker.com/)
- [Bootstrap](https://getbootstrap.com/)
- [jQuery](https://jquery.com/)
- [jQuery](https://jquery.com/)
- [Day.js](https://day.js.org/)
- [js-year-calendar](https://year-calendar.github.io/js-year-calendar/docs/index.html)
- [Tippy](https://atomiks.github.io/tippyjs/)
- [Coloris](https://coloris.js.org/)