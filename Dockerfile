FROM python:3.11

RUN apt-get update && apt-get install nginx -y --no-install-recommends

COPY ./nginx.default /etc/nginx/sites-available/default

WORKDIR /app

COPY ./requirements.txt /app
RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY . /app

EXPOSE 8000
STOPSIGNAL SIGTERM

CMD ["/app/start-server.sh"]


