#!/usr/bin/env bash
# start-server.sh
# Wait for db started
echo "Waiting for the db.."
python manage.py check --database default > /dev/null 2> /dev/null
until [ $? -eq 0 ];
do
  sleep 2
  python manage.py check --database default > /dev/null 2> /dev/null
  echo "Still waiting for the db.."
done
echo "Connected to database."
# Using 4 workers as default
WORKERS="${WORKERS:-4}"
# Migrate the last database changes
python manage.py migrate
# Collect the static files
python manage.py collectstatic --noinput -c
# Create a superuser if he does not exist
cat <<EOF | python manage.py shell
from django.contrib.auth import get_user_model

User = get_user_model()  # get the currently active user model,

User.objects.filter(username="$DASHBOARD_USER").exists() or \
    User.objects.create_superuser("$DASHBOARD_USER", '', "$DASHBOARD_USER_PASSWORD")
EOF

# Start the server
(uvicorn CounterDashboard.asgi:application --host 127.0.0.1 --port 8010 --workers $WORKERS --reload) &

nginx -g "daemon off;"